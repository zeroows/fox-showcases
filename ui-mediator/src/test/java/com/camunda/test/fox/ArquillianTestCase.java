package com.camunda.test.fox;

import static org.junit.Assert.*;

import java.util.HashMap;

import javax.inject.Inject;

import org.activiti.engine.HistoryService;
import org.activiti.engine.ProcessEngine;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.TaskService;
import org.activiti.engine.history.HistoricProcessInstance;
import org.activiti.engine.runtime.ProcessInstance;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.jboss.shrinkwrap.resolver.api.DependencyResolvers;
import org.jboss.shrinkwrap.resolver.api.maven.MavenDependencyResolver;
import org.junit.Test;
import org.junit.runner.RunWith;

@RunWith(Arquillian.class)
public class ArquillianTestCase {

  @Deployment  
  public static WebArchive createDeployment() {
    MavenDependencyResolver resolver = DependencyResolvers.use(MavenDependencyResolver.class).loadMetadataFromPom("pom.xml");
    
    return ShrinkWrap.create(WebArchive.class, "test.war")
            // prepare as process application archive for fox platform
            .addAsLibraries(resolver.artifact("com.camunda.fox.platform:fox-platform-client").resolveAsFiles())
            .addAsWebResource("META-INF/processes.xml", "WEB-INF/classes/META-INF/processes.xml")
            .addAsWebResource("META-INF/persistence.xml", "WEB-INF/classes/META-INF/persistence.xml")
            .addAsWebResource("META-INF/beans.xml", "WEB-INF/classes/META-INF/beans.xml")
            // add your own classes (could be done one by one as well)
            .addPackages(true, "com.camunda.sample")
            // add process definition
            .addAsResource("UI-Mediator-Pattern.bpmn20.xml")
            // now you can add additional stuff required for your test case
            ;    
  }

  @Inject
  private ProcessEngine processEngine;

  @Inject
  private RuntimeService runtimeService;

  @Inject
  private TaskService taskService;
  
  @Inject
  private HistoryService historyService;

  @Test
  public void test() throws InterruptedException {
    HashMap<String, Object> variables = new HashMap<String, Object>();
    
    ProcessInstance processInstance = runtimeService.startProcessInstanceByKey("CustomerRegistration");
    String id = processInstance.getId();
    System.out.println("Started process instance id " + id);
    
    // write test case for whole process scenario
//
//    HistoricProcessInstance historicProcessInstance = historyService.createHistoricProcessInstanceQuery().processInstanceId(id).singleResult();
//    assertNotNull(historicProcessInstance);
//
//    System.out.println("Finished, took " + historicProcessInstance.getDurationInMillis() + " millis");
       
  }
}
