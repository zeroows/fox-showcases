package com.camunda.fox.example.versicherung.service;

import java.math.BigDecimal;

import javax.enterprise.inject.Produces;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.activiti.cdi.BusinessProcess;

import com.camunda.fox.example.versicherung.entity.Angebot;

@Named
public class AngebotsService {
  
  @PersistenceContext
  private EntityManager em;
  
  @Inject
  private BusinessProcess businessProcess;
  
  public void angebotErstellen() {
    Angebot angebot = new Angebot();
    angebot.setFirstname((String) businessProcess.getVariable("vorname"));
    angebot.setLastname((String) businessProcess.getVariable("nachname"));    
    // berechne Monatsbeitrag 
    // (Hey, vielleicht sogar mit JBoss Drools!)
    BigDecimal montasbeitrag = new BigDecimal(100);    
    angebot.setMonatsbeitrag(montasbeitrag);
    
    em.persist(angebot);  
    em.flush();
    businessProcess.setVariable("angebotsId", angebot.getId());
  }
  
  @Produces @Named
  public Angebot angebot() {
    return em.find(Angebot.class, businessProcess.getVariable("angebotsId"));
  }  
}
