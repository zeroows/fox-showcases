package com.camunda.fox.example.versicherung.entity;

import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Version;

@Entity
public class Angebot {

  @Id
  @GeneratedValue
  private Long id;

  @Version
  private Long version;

  private String firstname;
  private String lastname;
  private BigDecimal monatsbeitrag;

  public String getFirstname() {
    return firstname;
  }

  public void setFirstname(String firstname) {
    this.firstname = firstname;
  }

  public String getLastname() {
    return lastname;
  }

  public void setLastname(String lastname) {
    this.lastname = lastname;
  }

  public BigDecimal getMonatsbeitrag() {
    return monatsbeitrag;
  }

  public String getMonatsbeitragString() {
    return monatsbeitrag.toPlainString();
  }

  public void setMonatsbeitrag(BigDecimal monatsbeitrag) {
    this.monatsbeitrag = monatsbeitrag;
  }

  public Long getId() {
    return id;
  }

  public Long getVersion() {
    return version;
  }

}
