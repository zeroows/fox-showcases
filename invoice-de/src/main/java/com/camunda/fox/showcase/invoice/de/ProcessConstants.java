package com.camunda.fox.showcase.invoice.de;


public class ProcessConstants {

  public static final String PROCESS_NAME_ADONIS = "adonis-invoice";
  public static final String PROCESS_NAME_FOX = "fox-invoice";
  public static final String PROCESS_NAME_SIGNAVIO = "signavio-invoice";
  public static final String PROCESS_NAME_IBO = "ibo-invoice";
  public static final String PROCESS_NAME_YAO = "yao-invoice";
  public static final String PROCESS_NAME_VP = "vp-invoice";
  public static final String PROCESS_NAME_EA = "ea-invoice";
  public static final String PROCESS_NAME_BPI = "bpi-invoice";
  
  public static final String VARIABLE_INVOICE = "invoice";

}
