package com.camunda.fox.mule;

import java.util.Map;
import java.util.logging.Logger;

import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.activiti.engine.RuntimeService;
import org.activiti.engine.runtime.ProcessInstance;

import com.camunda.fox.platform.api.ProcessEngineService;

public class FoxService {

	private final static Logger log = Logger.getLogger(FoxService.class.getName());
	
	private final String PROCESS_KEY = "open-account-mule";
	private ProcessEngineService processEngineService;
	private RuntimeService runtimeService;
	
	public FoxService() throws NamingException {
		log.info("Trying to look up process engine service from JNDI");
		processEngineService = (ProcessEngineService)new InitialContext().lookup("java:global/camunda-fox-platform/process-engine/PlatformService!com.camunda.fox.platform.api.ProcessEngineService");
		log.info("Successfully looked up process engine service from JNDI: " + processEngineService);
		
		// TODO: this will always retrieve the runtime service from the process 
		// instance with the name "default". Since this code is currently only 
		// intended as a simple show-case, multitenancy support is not considered!
		runtimeService = processEngineService.getProcessEngine("default").getRuntimeService();
	}

	// --- Convenience methods for showcase
	
	public void startAccontOpeningProcess(Map<String, Object> variables) {
		String businessKey = (String)variables.get("ordernumber");
		runtimeService.startProcessInstanceByKey(PROCESS_KEY, businessKey, variables);
	}

	public void postidentReceived(String businessKey) {
		
		// TODO: this should be done by a 
		// transformer/processor in the flow
		businessKey = businessKey.split("-")[1].substring(0, 4);
		
		ProcessInstance processInstance = runtimeService.createProcessInstanceQuery().processInstanceBusinessKey(businessKey, PROCESS_KEY).singleResult();
		runtimeService.signal(processInstance.getId());
	}
}
